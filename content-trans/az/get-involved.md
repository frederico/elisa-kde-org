---
getintouch: 'Tərtibatla bağlı müzakirələrin çoxu [Elisa poçt paylaşmı siyahısına](http://mail.kde.org/mailman/listinfo/)
  dail olur.

  Sadəcə qoşulun, salamlaşın və bizə nə ilə kömək etmək istədiyinizi söyləyin!

  '
layout: get-involved
menu:
  main:
    weight: 4
name: Elisa
title: İştirak edin
userbase: Elisa
---
Elisa'ya töhvə vermək istəyirsiniz? Bəzi maraqlı tapşırıqlar üçün [Phabricator](https://phabricator.kde.org/tag/elisa/) və ya [mənbə koduna göz gəzdirmək üçün](https://invent.kde.org/multimedia/elisa) saytına baxın.
