---
getintouch: '大多数与开发有关的讨论会在 [Elisa 邮件列表](http://mail.kde.org/mailman/listinfo/) 中进行。

  如果你有意帮助 Elisa 的开发，请加入该邮件列表，与我们建立联系。

  '
layout: get-involved
menu:
  main:
    weight: 4
name: Elisa
title: 参与工作
userbase: Elisa
---
如果你打算为 Elisa 贡献代码，请访问 [Phabricator 网站](https://phabricator.kde.org/tag/elisa/) 寻找相关任务，也可以 [浏览 Elisa 的源代码](https://invent.kde.org/multimedia/elisa)。
