---
getintouch: 'De flesta utvecklingsrelaterade diskussionerna äger rum på [Elisas e-postlista](http://mail.kde.org/mailman/listinfo/).

  Gå bara med, säg hej och tala om för oss vad du vill hjälpa till med.

  '
layout: get-involved
menu:
  main:
    weight: 4
name: Elisa
title: Börja delta
userbase: Elisa
---
Vill du bidra till Elisa? Ta en titt på [Phabricator](https://phabricator.kde.org/tag/elisa/) för några roliga uppgifter eller [bläddra i källkoden](https://invent.kde.org/multimedia/elisa).
