---
getintouch: 'Most development-related discussions take place on the [Elisa mailing
  list](http://mail.kde.org/mailman/listinfo/).

  Just join in, say hi and tell us what you would like to help us with!

  '
layout: get-involved
menu:
  main:
    weight: 4
name: Elisa
title: Machen Sie mit
userbase: Elisa
---
Want to contribute to Elisa? Check out [Phabricator](https://phabricator.kde.org/tag/elisa/) for some fun task or [browse the source code](https://invent.kde.org/multimedia/elisa).
