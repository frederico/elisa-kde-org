---
getintouch: 'A maioria das discussões relacionadas com o desenvolvimento têm lugar
  na [lista de correio do Elisa](http://mail.kde.org/mailman/listinfo/).

  Basta subscrever, dar as boas-vindas e dizer-nos no que gostaria de nos ajudar:

  '
layout: get-involved
menu:
  main:
    weight: 4
name: Elisa
title: Participe
userbase: Elisa
---
Deseja contribuir para o Elisa? Consulte o [Phabricator](https://phabricator.kde.org/tag/elisa/) para ver alguma tarefa divertida ou [navegue pelo código-fonte](https://invent.kde.org/multimedia/elisa).
