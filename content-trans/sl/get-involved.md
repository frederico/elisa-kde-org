---
getintouch: 'Večina debat na temo razvoja se dogaja na poštnems seznamu [Elisa mailing
  list](http://mail.kde.org/mailman/listinfo/).

  Pridružite se, pozdravite in nam povejte, kako bi nam radi pomagali!

  '
layout: get-involved
menu:
  main:
    weight: 4
name: Elisa
title: Bodite vpleteni
userbase: Elisa
---
Bi radi prispevali k Elisi? Preverite [Phabricator](https://phabricator.kde.org/tag/elisa/) za kako zabavno nalogo ali [pobrskajte po izvorni kodi](https://invent.kde.org/multimedia/elisa).
