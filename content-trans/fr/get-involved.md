---
getintouch: "La plupart des discussions concernant le développement se font sur [Elisa\
  \ mailing list](http://mail.kde.org/mailman/listinfo/).\nRejoignez nous simplement,\
  \ dites bonjour et dites nous ce que vous voudriez faire pour nous aider ! \n"
layout: get-involved
menu:
  main:
    weight: 4
name: Elisa
title: Impliquez-vous
userbase: Elisa
---
Vous voulez contribuer à Elisa ? Veuillez consulter [Phabricator](https://phabricator.kde.org/tag/elisa/) pour y trouver quelques tâches amusantes ou [parcourez le code source code](https://invent.kde.org/multimedia/elisa).
