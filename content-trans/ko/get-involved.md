---
getintouch: '대부분 개발 관련 토의는 [Elisa 메일링 리스트](http://mail.kde.org/mailman/listinfo/)에서
  진행됩니다.

  메일링 리스트에 간단한 인삿말과 함께 도울 수 있는 것을 알려 주세요!

  '
layout: get-involved
menu:
  main:
    weight: 4
name: Elisa
title: 참여하기
userbase: Elisa
---
Elisa에 기여하고 싶으신가요? [Phabricator](https://phabricator.kde.org/tag/elisa/)에 등록된 작업을 알아 보거나 [소스 코드를 탐색](https://invent.kde.org/multimedia/elisa)할 수 있습니다.
