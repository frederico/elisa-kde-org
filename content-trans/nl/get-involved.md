---
getintouch: 'De meeste discussies met betrekking op ontwikkeling vinden plaats op
  de [E-maillijst Elisa](http://mail.kde.org/mailman/listinfo/).

  Doe mee, zeg hallo en vertel ons wat u fijn vindt om ons mee te helpen!

  '
layout: get-involved
menu:
  main:
    weight: 4
name: Elisa
title: Doe mee
userbase: Elisa
---
Wilt u bijdragen aan Elisa? Bekijk [Phabricator](https://phabricator.kde.org/tag/elisa/) voor een kleine taak of [blader in de broncode](https://invent.kde.org/multimedia/elisa).
