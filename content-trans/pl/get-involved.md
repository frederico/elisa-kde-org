---
getintouch: 'Większość z rozmów dotyczących rozwoju odbywa się na [liście dyskusyjnej
  Elisy](http://mail.kde.org/mailman/listinfo/).

  Wystarczy, że dołączysz, przywitasz się i powiesz nam w czym chciałbyś nam pomóc!

  '
layout: get-involved
menu:
  main:
    weight: 4
name: Elisa
title: Współtwórz
userbase: Elisa
---
Chcesz współtworzyć Elisę? Zajrzyj na [Phabricatora](https://phabricator.kde.org/tag/elisa/), żeby zobaczyć jakie ciekawe zadania mamy do zrobienia for lub [przejżyj kod źródłowy](https://invent.kde.org/multimedia/elisa).
