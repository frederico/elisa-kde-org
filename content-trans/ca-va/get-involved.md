---
getintouch: 'La majoria dels debats relacionats amb el desenvolupament tenen lloc
  a la [llista de correu de l''Elisa](http://mail.kde.org/mailman/listinfo/).

  Uniu-vos-hi, digueu hola i expliqueu amb què vos agradaria ajudar!

  '
layout: get-involved
menu:
  main:
    weight: 4
name: Elisa
title: Col·laboreu-hi
userbase: Elisa
---
Voleu col·laborar amb l'Elisa? Reviseu el [Phabricator](https://phabricator.kde.org/tag/elisa/) cercant alguna tasca interessant o [exploreu el codi font](https://invent.kde.org/multimedia/elisa).
