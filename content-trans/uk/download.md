---
appstream: org.kde.elisa.desktop
flatpak: true
gear: true
layout: download
menu:
  main:
    weight: 2
name: Elisa
sources:
- description: '* [Крамниця Microsoft](https://www.microsoft.com/en-us/p/elisa/9pb5md7zh8tl)

    * [Виконуваний файл для Win64](https://binary-factory.kde.org/view/Windows%2064-bit/job/Elisa_Release_win64/)

    '
  icon: /assets/img/windows.png
  name: Windows
title: Отримати
---
