---
getintouch: 'Більшість пов''язаних із розробкою обговорень відбувається у [списку
  листування Elisa](http://mail.kde.org/mailman/listinfo/).

  Долучіться до нього, привітайтеся і повідомте нам про те, чим можете допомогти!

  '
layout: get-involved
menu:
  main:
    weight: 4
name: Elisa
title: Приєднатися до команди
userbase: Elisa
---
Хочете взяти участь у розвитку Elisa? Пошукайте на [Phabricator](https://phabricator.kde.org/tag/elisa/) якесь цікаве для вас завдання або [ознайомтеся із початковим кодом](https://invent.kde.org/multimedia/elisa).
