---
getintouch: 'La mayoría de las conversaciones relacionadas con el desarrollo tienen
  lugar en la [lista de distribución de Elisa](http://mail.kde.org/mailman/listinfo/).

  Únase a ella, salude y díganos en qué le gustaría ayudarnos.

  '
layout: get-involved
menu:
  main:
    weight: 4
name: Elisa
title: Implicarse
userbase: Elisa
---
¿Quiere colaborar con Elisa? Consulte [Phabricator](https://phabricator.kde.org/tag/elisa/) para encontrar alguna tarea divertida o [explore el código fuente](https://invent.kde.org/multimedia/elisa).
