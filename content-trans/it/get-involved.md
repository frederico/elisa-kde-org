---
getintouch: 'La maggior parte delle discussioni relative allo sviluppo si svolgono
  sulla [lista di distribuzione di Elisa] (http://mail.kde.org/mailman/listinfo/).

  Unisciti a noi, salutaci e dicci in cosa vorresti aiutarci!

  '
layout: get-involved
menu:
  main:
    weight: 4
name: Elisa
title: Partecipa
userbase: Elisa
---
Vuoi contribuire ad Elisa? Dai un'occhiata a [Phabricator](https://phabricator.kde.org/tag/elisa/) per qualche attività divertente o [naviga il codice sorgente](https://invent.kde.org/multimedia/elisa).
