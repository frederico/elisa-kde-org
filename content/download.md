---
title: Download
layout: download
appstream: org.kde.elisa.desktop
name: Elisa
gear: true
flatpak: true
menu:
  main:
    weight: 2
sources:
  - name: Windows
    icon: /assets/img/windows.png
    description: |
      * [Microsoft store](https://www.microsoft.com/en-us/p/elisa/9pb5md7zh8tl)
      * [Win64 executable](https://binary-factory.kde.org/view/Windows%2064-bit/job/Elisa_Release_win64/)
---
